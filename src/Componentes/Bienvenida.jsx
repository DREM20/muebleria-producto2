import React from 'react';

const Bienvenida = () => {
    return (
        <>
            <div className="container">
                <div className="jumbotron mt-5">
                    <h1 className="display-4">Gracias por iniciar sesion, ahora navega por nuestra pagina</h1>
                    <p className="lead">Se hace el uso del React para hacer esta pagina</p>
                    <hr className="my-4" />
                    <p>Control de Inventarios </p>
                </div>
            </div>

        </>
    );
}

export default Bienvenida